package com.springboot.schoolmanagement.entity.subject;

public enum SubjectEnum {
    PHYSIC(1, "Physic"),
    MATH(2, "Math Teacher"),
    ENGLISH(3, "English");

    private int code;
    private String subjectName;

    SubjectEnum(int code, String subjectName) {
        this.code = code;
        this.subjectName = subjectName;
    }
}
