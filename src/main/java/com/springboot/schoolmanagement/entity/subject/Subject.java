package com.springboot.schoolmanagement.entity.subject;

import com.springboot.schoolmanagement.entity.score.Score;
import com.springboot.schoolmanagement.utils.Generator;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Subject {
    @Id
    @Column(name = "subject_id", length = 5, nullable = false, updatable = false)
    private String subjectId = Generator.generateRandomCodeWithoutSpecialCharacters(5);
    @Column(name = "subject_name", length = 50, nullable = false)
    private String subjectName;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    private List<Score> scores;
}
