package com.springboot.schoolmanagement.entity.school;

import com.springboot.schoolmanagement.entity.level.Level;
import com.springboot.schoolmanagement.entity.teacher.Teacher;
import com.springboot.schoolmanagement.utils.Generator;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class School {
    @Id
    @Column(name = "school_id", length = 5, nullable = false, updatable = false)
    private String schoolId = Generator.generateRandomCodeWithoutSpecialCharacters(5);
    @Column(name = "school_name", length = 50, nullable = false)
    private String schoolName;

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL)
    private List<Level> levels;

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL)
    private List<Teacher> teachers;
}
