package com.springboot.schoolmanagement.entity.school;

import org.springframework.data.repository.CrudRepository;

public interface SchoolRepository extends CrudRepository<School, String> {
}
