package com.springboot.schoolmanagement.entity.dummy;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("api/data")
public class DummyDataController {
    private final DummyDataService dummyDataService;

    @PostMapping("/insert")
    public ResponseEntity<String> insertDummyData() {
        try {
            dummyDataService.insertDummyData();
            log.info("Insert demo data success");
            return ResponseEntity.ok("Demo data inserted successfully");
        } catch (Exception e) {
            log.error("Failed to insert demo data: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to insert demo data");
        }
    }
}
