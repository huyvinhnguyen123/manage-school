package com.springboot.schoolmanagement.entity.dummy;

import com.springboot.schoolmanagement.entity.level.Level;
import com.springboot.schoolmanagement.entity.level.LevelEnum;
import com.springboot.schoolmanagement.entity.level.LevelRepository;
import com.springboot.schoolmanagement.entity.school.School;
import com.springboot.schoolmanagement.entity.school.SchoolRepository;
import com.springboot.schoolmanagement.entity.score.Score;
import com.springboot.schoolmanagement.entity.score.ScoreRepository;
import com.springboot.schoolmanagement.entity.score.TestTypeEnum;
import com.springboot.schoolmanagement.entity.student.Student;
import com.springboot.schoolmanagement.entity.student.StudentRepository;
import com.springboot.schoolmanagement.entity.subject.Subject;
import com.springboot.schoolmanagement.entity.subject.SubjectEnum;
import com.springboot.schoolmanagement.entity.subject.SubjectRepository;
import com.springboot.schoolmanagement.entity.teacher.Teacher;
import com.springboot.schoolmanagement.entity.teacher.TeacherEnum;
import com.springboot.schoolmanagement.entity.teacher.TeacherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class DummyDataService {
    private final SchoolRepository schoolRepository;
    private final LevelRepository levelRepository;
    private final ScoreRepository scoreRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;
    private final SubjectRepository subjectRepository;

    /**
     * Insert deo data for queries
     */
    @Transactional
    public void insertDummyData() {
        List<School> schools =createListSchool();
        schoolRepository.saveAll(schools);
        log.info("Save schools success!");

        List<Level> levels = createListLevel();
        levelRepository.saveAll(levels);
        log.info("Save levels success!");

        List<School> updateSchoolsWithLevels =updateSchoolWithLevels(levels);
        schoolRepository.saveAll(updateSchoolsWithLevels);
        log.info("update schools with levels success!");

        List<Teacher> teachers = createListTeacher();
        teacherRepository.saveAll(teachers);
        log.info("Save teachers success!");

        List<School> updateSchoolsWithTeachers =updateSchoolWithTeachers(teachers);
        schoolRepository.saveAll(updateSchoolsWithTeachers);
        log.info("update schools with teachers success!");

        List<Student> students = createListStudent();
        studentRepository.saveAll(students);
        log.info("Save students success!");

        List<Subject> subjects = createListSubject();
        subjectRepository.saveAll(subjects);
        log.info("Save subjects success!");

        List<Score> scores = createListScore();
        scoreRepository.saveAll(scores);
        log.info("Save scores success!");

        List<Student> updateStudentsWithScores = updateStudentsWithScores(scores);
        studentRepository.saveAll(updateStudentsWithScores);
        log.info("update students with scores success!");

        List<Subject> updateSubjectsWithScores = updateSubjectsWithScores(scores);
        subjectRepository.saveAll(updateSubjectsWithScores);
        log.info("update subjects with scores success!");
    }

    private List<School> createListSchool() {
        List<School> schools = new ArrayList<>();
        for(int i = 1; i <= 5; i++){
            School school = new School();
            school.setSchoolName("school " + i);
            schools.add(school);
        }
        return schools;
    }

    private List<Level> createListLevel() {
        List<School> schools = (List<School>) schoolRepository.findAll();
        List<Level> levels = new ArrayList<>();

        for(int i = 0; i < schools.size(); i++) {
            Level level = new Level();
            level.setSchool(schools.get(i));

            LevelEnum levelEnum = LevelEnum.values()[i % LevelEnum.values().length];
            level.setLevelName(levelEnum.name());
            if(levelEnum.name().equals("elementary")){
                level.setLevelDuration(1);
            } else if(levelEnum.name().equals("junior high school")) {
                level.setLevelDuration(2);
            } else {
                level.setLevelDuration(3);
            }

            levels.add(level);
        }
        return levels;
    }

    private List<School> updateSchoolWithLevels(List<Level> levels) {
        List<Level> levelList = new ArrayList<>(levels);

        List<School> schools = (List<School>) schoolRepository.findAll();

        for(int i = 0; i < schools.size(); i++) {
            School existingSchool = schools.get(i);
            existingSchool.setLevels(levelList);
        }

        return schools;
    }

    private List<Teacher> createListTeacher() {
        List<School> schools = (List<School>) schoolRepository.findAll();
        List<Teacher> teachers = new ArrayList<>();

        for(int i = 0; i < schools.size(); i++) {
            Teacher teacher = new Teacher();
            teacher.setSchool(schools.get(i));
            teacher.setTeacherName("Teacher " + i);

            TeacherEnum teacherEnum = TeacherEnum.values()[i % TeacherEnum.values().length];
            teacher.setRole(teacherEnum.name());

            teachers.add(teacher);
        }

        return teachers;
    }

    private List<School> updateSchoolWithTeachers(List<Teacher> teachers) {
        List<Teacher> teacherList = new ArrayList<>(teachers);

        List<School> schools = (List<School>) schoolRepository.findAll();

        for(int i = 0; i < schools.size(); i++) {
            School existingSchool = schools.get(i);
            existingSchool.setTeachers(teacherList);
        }

        return schools;
    }

    private List<Student> createListStudent() {
        List<School> schools = (List<School>) schoolRepository.findAll();
        List<Level> levels = (List<Level>) levelRepository.findAll();
        List<Student> students = new ArrayList<>();

        for(int i = 0; i < schools.size(); i++) {
            Student student = new Student();
            student.setStudentName("Student " + i);
            for(Level l: levels) {
                student.setLevel(l);
            }
            students.add(student);
        }
        return students;
    }

    private List<Subject> createListSubject() {
        List<Subject> subjects = new ArrayList<>();
        for(int i = 1; i <= 3; i++){
            Subject subject = new Subject();

            SubjectEnum subjectEnum = SubjectEnum.values()[i % SubjectEnum.values().length];
            subject.setSubjectName(subjectEnum.name());

            subjects.add(subject);
        }
        return subjects;
    }

    private List<Score> createListScore() {
        List<Subject> subjects = (List<Subject>) subjectRepository.findAll();
        List<Student> students = (List<Student>) studentRepository.findAll();
        List<Score> scores = new ArrayList<>();

        Random random = new Random();
        double randomValue = random.nextDouble() * 10;
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        double roundedValue = Double.parseDouble(decimalFormat.format(randomValue));

        int index = 0;

        for(Subject sj: subjects) {
            for(Student sd: students) {
                Score score = new Score();
                score.setStudent(sd);
                score.setSubject(sj);

                TestTypeEnum testTypeEnum = TestTypeEnum.values()[index % TestTypeEnum.values().length];
                score.setTestType(testTypeEnum.name());

                if(testTypeEnum.name().equals("15 minutes")) {
                    score.setScore(roundedValue);
                } else if(testTypeEnum.name().equals("45 minutes")) {
                    score.setScore(roundedValue);
                } else {
                    score.setScore(roundedValue);
                }

                scores.add(score);

                index++;
            }
        }
        return scores;
    }

    public List<Student> updateStudentsWithScores(List<Score> scores) {
        List<Score> scoreList = new ArrayList<>(scores);

        List<Student> students = (List<Student>) studentRepository.findAll();

        for(int i = 0; i < students.size(); i++) {
            Student existingStudent = students.get(i);
            existingStudent.setScores(scoreList);
        }

        return students;
    }

    public List<Subject> updateSubjectsWithScores(List<Score> scores) {
        List<Score> scoreList = new ArrayList<>(scores);

        List<Subject> subjects = (List<Subject>) subjectRepository.findAll();

        for(int i = 0; i < subjects.size(); i++) {
            Subject existingSubject = subjects.get(i);
            existingSubject.setScores(scoreList);
        }

        return subjects;
    }
}
