package com.springboot.schoolmanagement.entity.teacher;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.schoolmanagement.entity.school.School;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Teacher {
    @Id
    @Column(name = "teacher_id", nullable = false, updatable = false)
    private String teacherId = UUID.randomUUID().toString();
    @Column(name = "teacher_name", length = 50, nullable = false)
    private String teacherName;
    @Column(name = "role", length = 50, nullable = false)
    private String role;

    @ManyToOne
    @JoinColumn(name = "school_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties
    private School school;
}
