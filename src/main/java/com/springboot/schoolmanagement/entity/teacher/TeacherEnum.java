package com.springboot.schoolmanagement.entity.teacher;

public enum TeacherEnum {
    PHYSIC_TEACHER(1, "Physic Teacher"),
    MATH_TEACHER(2, "Math Teacher"),
    ENGLISH_TEACHER(3, "English Teacher");

    private int code;
    private String teacherName;

    TeacherEnum(int code, String teacherName) {
        this.code = code;
        this.teacherName = teacherName;
    }
}
