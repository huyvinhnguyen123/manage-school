package com.springboot.schoolmanagement.entity.student;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StudentDTOAvg {
    private String studentId;
    private String studentName;
    private String score;
    private String subjectId;
}
