package com.springboot.schoolmanagement.entity.student;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("api/student")
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/history_top_gpa")
    public ResponseEntity<StudentDTOTopGPA> findStudentHistoryAndGPATop(@RequestParam String studentId) {
        log.info("Request find student top gpa...");
        return new ResponseEntity<>(studentService.findStudentHistoryAndGPATop(studentId), HttpStatus.OK);
    }

    @GetMapping("/avg-pass")
    public ResponseEntity<Iterable<StudentDTOAvg>> findStudentAvg(@RequestParam String subject1, @RequestParam String subject2, @RequestParam String subject3) {
        log.info("Request find student top gpa...");
        return new ResponseEntity<>(studentService.findStudentAvg(subject1, subject2, subject3), HttpStatus.OK);
    }
}
