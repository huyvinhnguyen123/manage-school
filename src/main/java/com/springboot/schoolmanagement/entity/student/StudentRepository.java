package com.springboot.schoolmanagement.entity.student;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, String> {
    @Query(value = "SELECT s.student_id, s.student_name, l.level_name, sc.school_name, AVG(score) AS gpa\n" +
            "FROM student s\n" +
            "JOIN level l ON s.level_id = l.level_id\n" +
            "JOIN school sc ON l.school_id = sc.school_id\n" +
            "JOIN score sc2 ON s.student_id = sc2.student_id\n" +
            "GROUP BY s.student_id, s.student_name, l.level_name, sc.school_name\n" +
            "HAVING s.student_id = ?1 \n" +
            "ORDER BY gpa DESC", nativeQuery = true)
    Optional<StudentDTOTopGPA> findStudentHistoryAndGPATop(String studentId);

    @Query(value = "SELECT s.student_id, s.student_name\n" +
            "FROM student s\n" +
            "JOIN score sc ON s.student_id = sc.student_id\n" +
            "WHERE sc.score >= 8.0\n" +
            "AND (\n" +
            "    sc.subject_id = ?1\n" +
            "    OR sc.subject_id = ?2\n" +
            "    OR sc.subject_id = ?3\n" +
            ")\n" +
            "GROUP BY s.student_id, s.student_name\n" +
            "HAVING MIN(sc.score) >= 6.5;", nativeQuery = true)
    List<StudentDTOAvg> findStudentAvg(String subject1, String subject2, String subject3);
}
