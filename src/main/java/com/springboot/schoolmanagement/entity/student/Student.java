package com.springboot.schoolmanagement.entity.student;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.schoolmanagement.entity.level.Level;
import com.springboot.schoolmanagement.entity.score.Score;
import com.springboot.schoolmanagement.entity.teacher.Teacher;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Student {
    @Id
    @Column(name = "student_id", nullable = false, updatable = false)
    private String studentId = UUID.randomUUID().toString();
    @Column(name = "student_name", length = 50, nullable = false)
    private String studentName;

    @ManyToOne
    @JoinColumn(name = "level_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties
    private Level level;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private List<Score> scores;
}
