package com.springboot.schoolmanagement.entity.student;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StudentDTOTopGPA {
    private String studentId;
    private String studentName;
    private String levelName;
    private String schoolName;
    private String levelId;
    private String schoolId;
}
