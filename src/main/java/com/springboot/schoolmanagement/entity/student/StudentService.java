package com.springboot.schoolmanagement.entity.student;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {
    private final StudentRepository studentRepository;

    public StudentDTOTopGPA findStudentHistoryAndGPATop(String studentId) {
        StudentDTOTopGPA existingStudent = studentRepository.findStudentHistoryAndGPATop(studentId).orElseThrow(() -> {
            log.error("Search this student fail");;
            return new NullPointerException("Not found this account: ");
        });
        log.info("Search success");
        return existingStudent;
    }

    public Iterable<StudentDTOAvg> findStudentAvg(String subject1, String subject2, String subject3) {
        Iterable<StudentDTOAvg> students = studentRepository.findStudentAvg(subject1, subject2, subject3);
        log.info("Search success");
        return students;
    }
}
