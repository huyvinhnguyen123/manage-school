package com.springboot.schoolmanagement.entity.score;

public enum TestTypeEnum {
    FIFTEEN_MINUTES(1, "15 minutes"),
    FOURTYFIVE_MINUTES(2, "45 minutes"),
    SEMESTER(3, "semester");

    private int code;
    private String subjectName;

    TestTypeEnum(int code, String subjectName) {
        this.code = code;
        this.subjectName = subjectName;
    }
}
