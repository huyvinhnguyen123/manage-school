package com.springboot.schoolmanagement.entity.score;

import org.springframework.data.repository.CrudRepository;

public interface ScoreRepository extends CrudRepository<Score, String> {
}
