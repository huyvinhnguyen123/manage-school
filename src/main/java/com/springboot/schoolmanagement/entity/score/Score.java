package com.springboot.schoolmanagement.entity.score;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.schoolmanagement.entity.student.Student;
import com.springboot.schoolmanagement.entity.subject.Subject;
import com.springboot.schoolmanagement.utils.Generator;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Score {
    @Id
    @Column(name = "score_id", nullable = false, updatable = false)
    private String scoreId = Generator.generateRandomCodeWithoutSpecialCharacters(10);
    @Column(name = "test_type", nullable = false)
    private String testType;
    @Column(name = "score", nullable = false)
    private double score;

    @ManyToOne
    @JoinColumn(name = "student_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties
    private Student student;

    @ManyToOne
    @JoinColumn(name = "subject_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties
    private Subject subject;
}
