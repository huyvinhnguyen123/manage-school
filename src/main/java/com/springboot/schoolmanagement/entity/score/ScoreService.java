package com.springboot.schoolmanagement.entity.score;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ScoreService {
    private final ScoreRepository scoreRepository;

    public Score findScoreById(String scoreId) {
        Score existingScore = scoreRepository.findById(scoreId).orElseThrow(() -> {
            log.error("Search this score fail");;
            return new NullPointerException("Not found this account: ");
        });
        log.info("Get score success");
        return existingScore;

    }
}
