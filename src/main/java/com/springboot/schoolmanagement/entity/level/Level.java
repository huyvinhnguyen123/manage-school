package com.springboot.schoolmanagement.entity.level;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.schoolmanagement.entity.school.School;
import com.springboot.schoolmanagement.utils.Generator;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Level {
    @Id
    @Column(name = "level_id", length = 5, nullable = false, updatable = false)
    private String levelId = Generator.generateRandomCodeWithoutSpecialCharacters(5);
    @Column(name = "level_name", length = 50, nullable = false)
    private String levelName;
    @Column(name = "level_duration", length = 1, nullable = false)
    private int levelDuration;

    @ManyToOne
    @JoinColumn(name = "school_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties
    private School school;
}
