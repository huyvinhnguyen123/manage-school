package com.springboot.schoolmanagement.entity.level;

public enum LevelEnum {
    ELEMENTARY(1, "elementary"),
    JUNIOR_HIGH_SCHOOL(2, "junior high school"),
    HIGH_SCHOOL(3, "high school");

    private int levelId;
    private String levelName;

    LevelEnum(int levelId, String levelName) {
        this.levelId = levelId;
        this.levelName = levelName;
    }

}
