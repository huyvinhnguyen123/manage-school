package com.springboot.schoolmanagement.entity.level;

import org.springframework.data.repository.CrudRepository;

public interface LevelRepository extends CrudRepository<Level, String> {
}
