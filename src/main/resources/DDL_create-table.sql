BEGIN;


CREATE TABLE IF NOT EXISTS public.level
(
    level_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    level_duration integer NOT NULL,
    level_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    school_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT level_pkey PRIMARY KEY (level_id)
);

CREATE TABLE IF NOT EXISTS public.school
(
    school_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    school_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT school_pkey PRIMARY KEY (school_id)
);

CREATE TABLE IF NOT EXISTS public.score
(
    score_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    score double precision NOT NULL,
    test_type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    student_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    subject_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT score_pkey PRIMARY KEY (score_id)
);

CREATE TABLE IF NOT EXISTS public.student
(
    student_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    student_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    level_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT student_pkey PRIMARY KEY (student_id)
);

CREATE TABLE IF NOT EXISTS public.subject
(
    subject_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    subject_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT subject_pkey PRIMARY KEY (subject_id)
);

CREATE TABLE IF NOT EXISTS public.teacher
(
    teacher_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    role character varying(50) COLLATE pg_catalog."default" NOT NULL,
    teacher_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    school_id character varying(5) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT teacher_pkey PRIMARY KEY (teacher_id)
);

ALTER TABLE IF EXISTS public.level
    ADD CONSTRAINT fkpjmn3h80d288rq7idikol4ad3 FOREIGN KEY (school_id)
    REFERENCES public.school (school_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE IF EXISTS public.score
    ADD CONSTRAINT fk56nv285e8l73fru4sw2152y87 FOREIGN KEY (subject_id)
    REFERENCES public.subject (subject_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE IF EXISTS public.score
    ADD CONSTRAINT fknap51mbove93yjb09idc9jic6 FOREIGN KEY (student_id)
    REFERENCES public.student (student_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE IF EXISTS public.student
    ADD CONSTRAINT fkhouc0qse1o4wn2emncf5spsru FOREIGN KEY (level_id)
    REFERENCES public.level (level_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE IF EXISTS public.teacher
    ADD CONSTRAINT fkrg46bnmgbcccayv14naymqg3r FOREIGN KEY (school_id)
    REFERENCES public.school (school_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;

END;